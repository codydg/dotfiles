#!/bin/bash

yesno() {
    read -p "$1 " -n 1 -r
    echo
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        false
    fi
    return
}

dotdir="$(dirname "$(realpath -- "$0")")"
yesno "dotFiles was found to be at \"$dotdir\". Is this correct?" || exit 1

ln -si "$dotdir/.bashrc" "$HOME/.bashrc" || ln -si "$dotdir/.bashrc" "$HOME/.bash_aliases"

for fn in .inputrc .tmux.conf .vimrc .dmrc; do
    ln -si "$dotdir/$fn" "$HOME/$fn"
done

mkdir -p "$HOME/.config"
for fn in $(find "$dotdir/.config/" -maxdepth 1 -mindepth 1); do
    homeloc="$HOME/.config/$(basename "$fn")"
    mkdir -p "$homeloc"
    find "$fn" -maxdepth 1 -mindepth 1 -exec ln -si "{}" "$homeloc" \;
done

# This could technically be combined with the line that makes the .config directory in the first place, but for the sake of clarity, it's kept separate.
mkdir -p "$HOME/.config/bash/additional"

