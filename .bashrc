# Some table flips for your everyday needs
#echo '(╯°□°）╯︵ ┻━┻)'
#echo '(╯°Д°)╯︵/(.□ . \)'
#echo '┻━┻︵ \(°□°)/ ︵ ┻━┻'
#echo '(ง •̀_•́)ง   o(-`д´- ｡)'

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     isLin=true;;
    CYGWIN*)    isLin=false;;
    MINGW*)     isLin=false;;
    *)          echo "Unsupported environment!"; return
esac

# Documents Path
if [ "$isLin" = true ]; then
    documents="$HOME/Documents"
    startfunc="gio open"
else
    documents=$(powershell "[Environment]::GetFolderPath('MyDocuments')" | sed 's-\\-/-g')
    startfunc="start"
fi

# Exports
export PATH="$HOME/.config/bash/scripts:"$PATH
if [ "$isLin" = true ]; then
    export PS1="\e[0;32m[\u@\h]\e[m \e[1;33m\w\e[m \e[0;33m\$(. DirStack.sh)\e[m\e[0;36m\$(Branch.sh)\e[m\n\$ "
fi

# Jump Aliases
alias cdgs="cd \"$documents/growingSTEMS\""

# System Configuration
# cbash used to be an alias...
cbash() {
    for testloc in .bash_aliases .bashrc; do
        fullpath="$HOME/$testloc"
        if [ -f "$fullpath" ]; then
            vim "$fullpath"
            return
        fi
    done
    echo "Error: Cannot find Bash configuration file!"
}
alias sourcebash="source \"$HOME/.bashrc\""
alias cvim="vim \"$HOME/.vimrc\""
alias ccoc="vim \"$HOME/.vim/coc-settings.json\""
alias ctmux="vim \"$HOME/.tmux.conf\""
alias cssh="vim \"$HOME/.ssh/config\""
alias ci3="vim \"$HOME/.config/i3/config\""
alias enablei3log="i3-msg 'debuglog on; shmlog on; reload'"
alias cala="vim \"$HOME/.config/alacritty/alacritty.yml\""

# Navigation Aliases
alias ls='ls --color=auto'
alias ll='ls -lh --color=auto'
alias la='ls -a --color=auto'
alias lla='ls -lah --color=auto'
alias fixd='cd && cd -'

# Java Dev Aliases
alias javadoc="$startfunc ./build/docs/javadoc/index.html"
alias unittestreport="$startfunc ./build/reports/tests/test/index.html"

# C++ Dev Aliases
alias c='./configure.sh'
alias b='./build.sh'
alias t='./test.sh'
alias cb='c && b'
alias bt='b && t'
alias cbt='c && b && t'

# Git Aliases
alias gitl_less='git log --decorate --oneline --graph --date=short --format="%C(auto)%h%d %s %C(cyan)%ad %C(magenta bold)%an"'
alias gitl='gitl_less --branches --all'
alias gits='git status'
alias gitsu='git submodule update --init --recursive'
alias gitd='git diff'
alias gitds='git diff --staged'
alias gitf='git fetch --prune'
alias gitp='git pull --prune'
alias gitpd='gitp origin dev'
alias gitb='git branch'
alias gitbdall='git branch -d $(git branch | grep -v "*")'

alias diff='git diff --no-index --color --'

# Tmux Aliases
alias tmux='tmux new -As0'

# Functions

npp()
{
    for testloc in "C:/Program Files/Notepad++/notepad++.exe" "C:/Program Files (x86)/Notepad++/notepad++.exe"; do
        if [ -f "$testloc" ]; then
            "$testloc" $@&
            return
        fi
    done
    echo "Error: Cannot find notepad++ executable!"
}

cdls() {
    cd $1
    ls
}

mkcd() {
    mkdir $1
    cd $1
}

function format()
{
    find -type f -iname "*.c" -o -iname "*.cpp" -o -iname "*.h" -o -iname "*.hpp" | xargs -P 0 -I {} clang-format -i $@ {}
}

function tidy()
{
    find -type f -iname "*.c" -o -iname "*.cpp" -o -iname "*.h" -o -iname "*.hpp" | xargs -I {} clang-tidy $@ {}
}

for f in $HOME/.config/bash/additional/*.sh; do
    [ -e "$f" ] || continue
    source "$f"
done

