#!/bin/bash
stackSize=$(dirs -v | wc -l)
if [[ $stackSize -gt 1 ]]; then
    ((stackSize--))
    echo -n "[$stackSize] "
fi

