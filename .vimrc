"disable modelines (security)
set nomodeline

"set leader character
let mapleader="."

"""""""""""""""""""""""""""""""""""
"GENERAL SETTINGS
"""""""""""""""""""""""""""""""""""
"make vim more useable
set nocompatible "disable vi compatibilities
filetype plugin indent on "filetype detection, filetype specific plugins, filetype indent
syntax on "syntax highlighting
set splitbelow splitright "split to the right and below instead of left and above

"encodeing
set encoding=utf-8

"De-clutter
set nobackup
set noswapfile

"Line numbers
set number relativenumber
set numberwidth=4

"Tabs, indents, etc
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
set smarttab

"search
set hlsearch
set incsearch

"no word wrap
set nowrap

"Code folding
set foldenable
set foldmethod=indent
set foldlevel=99

"showing filename when only one file is open
set laststatus=2
set statusline="%f"

set backspace=indent,eol,start
set number
set ruler
set hlsearch
set background=dark

"""""""""""""""""""""""""""""""""""
"PLUGINS
"""""""""""""""""""""""""""""""""""

if ! empty(globpath(&rtp, 'autoload/plug.vim'))
    call plug#begin()
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'rhysd/vim-clang-format'
    Plug 'zchee/vim-flatbuffers'
    Plug 'lluchs/vim-wren'
    Plug 'lepture/vim-jinja'
    Plug 'bfrg/vim-cpp-modern'
    Plug 'cespare/vim-toml'
    Plug 'mracos/mermaid.vim'
    call plug#end()
endif

set completeopt-=preview

"configure CoC
set completeopt-=preview
"cycle through completion list forward
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"cycle through completion list backward
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"get diagnostic info
nnoremap gt <Plug>(coc-diagnostic-info)
"format whole document
nnoremap gf :ClangFormat<CR>
"bring up fix it menu
nnoremap gF <Plug>(coc-fix-current)
"go-to-definition or declaration?
nnoremap gd <Plug>(coc-definition)
"go-to-references
nnoremap gr <Plug>(coc-references)
"navigate diagnostics
nnoremap gp <Plug>(coc-diagnostic-prev)
nnoremap gn <Plug>(coc-diagnostic-next)
"Rename symbol
nnoremap <F2> <Plug>(coc-rename)

"""""""""""""""""""""""""""""""""""
"AUTO COMMANDS
"""""""""""""""""""""""""""""""""""
"set filetypes that vim doesn't automatically detect
autocmd! BufNewFile,BufRead *.ino setlocal ft=arduino "arduino files
autocmd! BufNewFIle,BufRead *.cprj setlocal ft=vim "custom made project files are actualy just vimscript

"disable auto insertion of comments in all files
autocmd! FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"automatically delete trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

